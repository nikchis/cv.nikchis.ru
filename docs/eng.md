![Profile](media/profile.jpg)

Software engineer with 15+ years of experience.

My technical background and strong creative problem solving skills help me to implement solutions for every new challenge.

I like to build awesome software & I have experience in desktop, web applications, server-side services design, development and deployment.

I adhere to the rule that every complex architectural decision should be well documented and described using a set of diagrams.

As for programming languages, currently I prefer Golang for most server-side tasks, C  when solution needs better performance, and Python mainly for research tasks and test scripts.

I have experience in database design, normalization, creating schemas and migrations, setting up replication.

Also I can prepare Docker images, set up CI/CD pipelines, interact with K8s, create manifests and Helm charts for deployment.


### Education

* 2009 - 2013 : Omsk Branch of Sobolev Institute of Mathematics, Siberian Branch of the Russian Academy of Sciences, Laboratory of Mathematical Modeling, Numerical Methods and Software Complexes, PhD student

* 2004 - 2009 : Omsk State University (OMSU), Faculty of Mathematics, Specialist

### Experience

* Dec 2023 - Present : Technical Lead at Samokat.tech (E-com group)
	+ Advertising Platform development
		* microservices architecture
		* Golang
		* REST API, Apache Kafka
		* PostgreSQL, MSSQL, ClickHouse, Elasticsearch, Redis
		* PlantUML, DocHub
		* Gateway, BFF, CQRS, Transaction log tailing, Event sourcing
		* OpenTelemetry, Prometheus, Grafana, Jaeger
		* Gitlab CI/CD, Docker, K8s, Helm

* Jan 2023 - Dec 2023 : Senior Software Engineer at Megamarket (E-com group)
	+ Advertising Platform development
		* microservices architecture
		* Golang
		* REST API, gRPC, Apache Kafka
		* PostgreSQL, MSSQL, Elasticsearch, Redis
		* Gateway, BFF
		* PlantUML, DocHub
		* OpenTelemetry, Prometheus, Grafana, Jaeger
		* Gitlab CI/CD, Docker, K8s, Helm

* Mar 2021 - Sep 2022 : Technical Lead at Stellar Solvers LLC
	+ AI solution development for spice manufacturer
		* microservices architecture
		* Golang, C, Python, ReactJS
		* Websocket, NNG/Mangos
		* Darknet Neural Network framework
		* PaddleOCR inference
		* Bare Metal Kubernetes cluster
		* ArgoCD, GitOps, Gitlab CI/CD, Docker
			
	+ AI, AR real-time solution for coffee machine manufacturer
		* microservices architecture
		* Golang, C, Python, ReactJS
		* WebRTC, Websocket, NNG/Mangos
		* VP8, VP9 codecs frame extraction & transformation
		* Darknet Neural Network framework
		* Points of Interest Tracker
		* Grafana Loki
		* Gitlab CI/CD, Docker образы
	
	+ Implementation of an Enterprise Data Warehouse (EDW) for an international corporation
		* Python, Apache Airflow DAGs, Apache Kafka
		* Extract, Load, Transform (ELT), CSV, Parquet
		* Google Cloud: Cloud Storage, Kubernetes (GKE), Big Query, Google Analytics
		* Oracle
		* Jira
		
	+ Development of Real Time Bidding (RTB) solution on a programmatic-platform
		* microservices architecture
		* Golang
		* Python, Jupyter Lab
		* Reinforcement Learning
		* Extract, Transform, Load (ETL), CSV, Apache Avro
		* GCP Big Query, ClickHouse
		* Google Cloud: Cloud Storage, Compute Engine, App Engine
		* GitHub Actions (CI/CD)
		* Jira

* Jun 2021 - Apr 2022 : Software Engineer at IntelliBoard, Inc
	+ Predictive Analytics Platform development
		* microservices architecture
		* Golang, Python
		* REST API, RabbitMQ
		* Amazon RDS
		* Gitlab CI/CD
		* Jira, Confluence

* Jun 2020 - Mar 2021 : Technical Lead at Sebale LLC
	+ ML Annotation Platform development and deployment
		* microservices architecture
		* Golang, C, Python
		* Websocket, NNG/Mangos, RabbitMQ
		* Gitlab CI/CD, Docker
		* PostgreSQL, ClickHouse
		* Redis
		* Nginx
		* Oracle Cloud Infrastructure (OCI)
		
	+ Prepare VMs for Qt application cross-compilation, building and deployment
		* Virtualbox (Win, MacOS, Linux), Qemu/KVM (Win, Linux)
		* Building scripts development: Qmake, Bash
		* Gitlab CI/CD
			
	+ WebAR solution quality assessment
		* Code review
		* UML diagrams creation
		* Architecture improvement
		* Load testing
		* Bitbucket
		* AWS EC2

* May 2020 - Jun 2020 : Software Engineer at Effective LLC
	+ Sound processing solution development
		* microservices architecture
		* Golang, C
		* REST API, gRPC
		* Gitlab CI/CD, Docker

* Feb 2020 - May 2020 : Software Engineer at Stellar Solvers LLC
	+ AI (CV) ("shelf checking") Retail solution development
		* microservices architecture
		* Golang, C, C++, Python
		* PostgreSQL
		* Redis
		* PyTorch
		* REST API, ZeroMQ
		* Gitlab CI/CD, Docker
		* AWS EC2, AWS S3, AWS EFS
		* Ansible
		* Trello

* Oct 2018 - Feb 2020 : Software Developer at Blocknet
	+ Trading Platform data processing software development
		* Extract, Transform, Load (ETL)
		* Golang
		* PostgreSQL, Redis Cluster

	+ Cross-communication solution development
		* Golang
		* PostgreSQL, Redis Cluster
		* VPS (Vultr)
		
* May 2011 - Mar 2018 : Software Engineer at Innoway LLC
	+ CRM software development
		* Delphi
		* Devart MyDAC, UniDAC
		* DevExpress
		* MySQL, SQLite
		
	+ Web application development
		* PHP
		* JS, jQuery
		* MySQL

* Oct 2006 - Feb 2011 : Programmer at Omsk Branch of Sobolev Institute of Mathematics, 
Siberian Branch of the Russian Academy of Sciences
	+ Development and maintaining of internal billing system
		* Delphi
		* DevExpress
		* Devart ODAC
		* PL/SQL
		* Oracle

### Skills

* Software design
	+ DDD, SOLID principles, Microservices, Distributed systems
	+ Gateway, BFF, CQRS, Event sourcing, Saga
	+ Machine Learning, Computer Vision, Neural Network Architectures

* Programming
	+ Golang, C, Python

* Data storage 
	+ PostgreSQL, MSSQL, Oracle, MySQL
	+ ClickHouse, BigQuery
	+ Redis, Elasticsearch

* Data transfer
	+ REST API, gRPC, Websocket
	+ Apache Kafka, RabbitMQ, NNG/Mangos, ZMQ

* Data format
	+ JSON, CBOR, Protobuf, Apache Avro, Apache Parquet

* Observability
	+ OpenTelemetry, Prometheus, Jaeger, Grafana

* Integration & Deployment
	+ Docker, Helm
	+ Gitlab CI/CD, GitHub Actions
	+ GitOps, ArgoCD

* Infrastructure
	+ AWS, GCP, OCI
	+ Kubernetes, Ansible
	+ Ubuntu, Debian, Alpine

### Contact

* LinkedIn: [@nikchis](https://linkedin.com/in/nikchis)
* Telegram: [@nik_chis](https://t.me/nik_chis)
* Skype: [live:.cid.b9ce95d0faeca035](https://join.skype.com/invite/b8hD12tUH2Ul)
