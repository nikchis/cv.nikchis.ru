![Profile](media/profile.jpg)

Инженер с опытом 15+ лет.

Мои технические навыки и творческие способности помогают мне реализовывать решения для каждой новой задачи.

Мне нравится создавать отличное программное обеспечение, и у меня имеется опыт проектирования, разработки и развертывания настольных приложений, веб приложений и серверных решений.

Я придерживаюсь правила, что каждое сложное архитектурное решение должно быть хорошо документировано и описано с помощью набора диаграмм.

Могу использовать различные языки программирования, в настоящее время я предпочитаю Golang для большинства серверных задач, C - когда для решения нужна более высокая производительность, и Python в основном для исследовательских работ и скриптов тестирования.

Способен подготавливать Docker образы, настраивать CI/CD для сборки и доставки приложений, работать с Kubernetes, создавать манифесты и Helm-чарты для развертывания.

Также обладаю опытом проектирования баз данных, нормализации, создания схем, миграций, работы как с классическими реляционными СУБД такими как PostgreSQL, так и со столбцовыми, например BigQuery, ClickHouse.

### Образование

* 2009 - 2013 : Омский филиал Федерального государственного бюджетного учреждения науки Института математики им. С.Л. Соболева Сибирского отделения Российской академии наук (ОФ ИМ СО РАН), Лаборатория математического моделирования, численных методов и комплексов программ, аспирантура 

* 2004 - 2009 : Омский Государственный Университет (ОмГУ), Математический факультет, специалитет

### Опыт работы

* Dec 2023 - Present : Technical Lead at Samokat.tech (E-com)
	+ Проектирование и разработка Advertising Platform
		* микросервисная архитектура
		* Golang
		* REST API, Apache Kafka
		* PostgreSQL, MSSQL, ClickHouse, Elasticsearch, Redis
		* PlantUML, DocHub
		* Gateway, BFF, CQRS, Transaction log tailing, Event sourcing
		* OpenTelemetry, Prometheus, Grafana, Jaeger
		* Gitlab CI/CD, Docker, K8s, Helm

* Jan 2023 - Dec 2023 : Senior Software Engineer at Megamarket (E-com)
	+ Проектирование и разработка Advertising Platform
		* микросервисная архитектура
		* Golang
		* REST API, gRPC, Apache Kafka
		* PostgreSQL, MSSQL, Elasticsearch, Redis
		* Gateway, BFF
		* PlantUML, DocHub
		* OpenTelemetry, Prometheus, Grafana, Jaeger
		* Gitlab CI/CD, Docker, K8s, Helm

* Mar 2021 - Sep 2022 : Technical Lead at Stellar Solvers LLC
	+ Разработка AI решения для производителя специй
		* микросервисная архитектура
		* Golang, C, Python, ReactJS
		* Websocket, NNG/Mangos
		* Нейронные сети на Darknet Neural Network framework
		* PaddleOCR инференс
		* Bare Metal Kubernetes кластер
		* ArgoCD, GitOps, Gitlab CI/CD, Docker
			
	+ Разработка AI, AR real-time решения для производителя кофемашин
		* микросервисная архитектура
		* Golang, C, Python, ReactJS
		* WebRTC, Websocket, NNG/Mangos
		* VP8, VP9 codecs frame extraction & transformation
		* Нейронные сети на Darknet Neural Network framework
		* Решение для отслеживания точек на кадрах (Points of Interest Tracker)
		* Grafana Loki
		* Gitlab CI/CD, Docker образы
	
	+ Проектирование и развертывание Enterprise Data Warehouse (EDW) решения для 
	транснациональной производственной компании
		* создание UML диаграмм
		* Python, Apache Airflow DAGs, Apache Kafka
		* Extract, Load, Transform (ELT), CSV, Parquet
		* Google Cloud: Cloud Storage, Kubernetes (GKE), Big Query, Google Analytics
		* Oracle
		* Jira
		
	+ Разработка и развертывание Real Time Bidding (RTB) решения 
	для одной из крупнейших рекламных programmatic-платформ
		* микросервисная архитектура
		* Golang
		* Python, Jupyter Lab
		* Reinforcement Learning оптимизационный алгоритм
		* Extract, Transform, Load (ETL), CSV, Apache Avro
		* GCP Big Query, ClickHouse
		* Google Cloud: Cloud Storage, Compute Engine, App Engine
		* GitHub Actions (CI/CD)
		* Jira

* Jun 2021 - Apr 2022 : Software Engineer at IntelliBoard, Inc
	+ Проектирование и разработка платформы предиктивной аналитики
		* микросервисная архитектура
		* Golang, Python
		* REST API, RabbitMQ
		* PyTorch тренировка, инференс нейронных сетей
		* Amazon RDS
		* Gitlab CI/CD
		* Jira, Confluence

* Jun 2020 - Mar 2021 : Technical Lead at Sebale LLC
	+ Разработка и развертывание серверной части средства для аннотирования изображений
		* микросервисная архитектура
		* Golang, C, Python
		* Websocket, NNG/Mangos, RabbitMQ
		* Gitlab CI/CD, Docker
		* PostgreSQL, ClickHouse
		* Redis
		* Nginx
		* Oracle Cloud Infrastructure (OCI)
		
	+ Подготовка виртуальных машин для сборки Qt приложений под различные платформы
		* Virtualbox (Win, MacOS, Linux), Qemu/KVM (Win, Linux)
		* разработка скриптов сборки и упаковки Qmake, Bash
		* Gitlab CI/CD
			
	+ Экспертиза команды и внутреннего продукта основанного на WebAR технологиях
		* код ревью
		* создание UML диаграмм
		* тестирование
		* модификация архитектуры
		* Bitbucket
		* AWS EC2

* May 2020 - Jun 2020 : Software Engineer at Effective LLC
	+ Разработка и развертывание сервиса для процессинга и модификации аудиофайлов
		* микросервисная архитектура
		* Golang, C
		* REST API, gRPC
		* Gitlab CI/CD, Docker образы

* Feb 2020 - May 2020 : Software Engineer at Stellar Solvers LLC
	+ Разработка и развертывание AI (CV) решения ("shelf checking") 
	для одного из крупнейших операторов ритейла
		* микросервисная архитектура
		* Golang, C, C++, Python
		* PostgreSQL основная база, реплики для BI 
		* Redis кэширование и конфигурация
		* PyTorch тренировка, инференс нейронных сетей
		* REST API, ZeroMQ
		* Gitlab CI/CD, Docker
		* AWS EC2, AWS S3, AWS EFS
		* Ansible
		* Trello
	

* Oct 2018 - Feb 2020 : Software Developer at Blocknet
	+ Разработка, развертывание, сопровождение платформы единого информационного пространства сообщества
		* Golang
		* PostgreSQL, Redis Cluster
		* VPS (Vultr)
		
	+ Разработка, развертывание средства для обработки данных биржевой торговли
		* Extract, Transform, Load (ETL)
		* Golang
		* PostgreSQL, Redis Cluster
		
	+ Разработка, развертывание ботов и приложений в Telegram, Slack, Discord
		* Golang
		* PostgreSQL, Redis Cluster
		* Websocket

* May 2011 - Mar 2018 : Software Engineer at Innoway LLC
	+ Разработка и сопровождение CRM систем для ритейла
		* Delphi
		* Devart MyDAC, UniDAC
		* DevExpress
		* MySQL, SQLite
		
	+ Разработка веб-приложений
		* PHP
		* JS, jQuery
		* MySQL

* Oct 2006 - Feb 2011 : Программист в Омском филиале 
	Института математики им. С.Л. Соболева 
	Сибирского отделения Российской академии наук (ОФ ИМ СО РАН)
	+ Разработка и сопровождение биллинговой системы
		* Delphi
		* DevExpress
		* Devart ODAC
		* PL/SQL
		* Oracle

### Языки

* English - upper intermediate (B2)
* Russian - native


### Основные навыки

* Software design
	+ DDD, SOLID principles, Microservices, Distributed systems
	+ Gateway, BFF, CQRS, Event sourcing, Saga
	+ Machine Learning, Computer Vision, Neural Network Architectures

* Programming
	+ Golang, C, Python

* Data storage 
	+ PostgreSQL, MSSQL, Oracle, MySQL
	+ ClickHouse, BigQuery
	+ Redis, Elasticsearch

* Data transfer
	+ REST API, gRPC, Websocket
	+ Apache Kafka, RabbitMQ, NNG/Mangos, ZMQ

* Data format
	+ JSON, CBOR, Protobuf, Apache Avro, Apache Parquet

* Observability
	+ OpenTelemetry, Prometheus, Jaeger, Grafana

* Integration & Deployment
	+ Docker, Helm
	+ Gitlab CI/CD, GitHub Actions
	+ GitOps, ArgoCD

* Infrastructure
	+ AWS, GCP, OCI
	+ Kubernetes, Ansible
	+ Ubuntu, Debian, Alpine

### Контакты

* Phone: [+7-913-157-4928](tel:+79131574928)
* LinkedIn: [@nikchis](https://linkedin.com/in/nikchis)
* Telegram: [@nik_chis](https://t.me/nik_chis)
* Skype: [live:.cid.b9ce95d0faeca035](https://join.skype.com/invite/b8hD12tUH2Ul)
